package com.rizato.rupdates;

/*
Copyright 2016 Robert Lathrop

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.rizato.rupdates.database.DbContract;
import com.rizato.rupdates.nano.Rupdates;
import com.rizato.rupdates.nano.UpdatesGrpc;
import com.rizato.rupdates.services.RegistrationService;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;

/**
 * This is the main activity. It kicks off the registration with the server for notifications, as
 * well as displays a list of updates from Rhialto posted to reddit. If a user clicks any item in
 * the list it will open up the mobile version of the comments page.
 *
 * The recyclerview has an attach swipe to refresh, with a refresh icon in the action bar for
 * accessibility.
 *
 * In addition this page populates an overflow menu with the privacy policy and the settings page.
 *
 */
public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>,
        SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String TOKEN = "token";
    public static final String NOTIFICATIONS = "notifications_enabled";
    private static final String TAG = MainActivity.class.getSimpleName();
    private PostsAdapter mAdapter;
    private SwipeRefreshLayout mSwipe;
    private boolean isRefreshing = false;
    private boolean isRegistered;
    private Handler mHandler;
    private static final int REQUEST_CODE = 3856;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            refreshList();
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        isRegistered = prefs.getBoolean(NOTIFICATIONS, true);
        prefs.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initHandler();
        initActionBar();
        initRecyclerView();
        initTokenIfNeeded();
        initSwipeRefresh();
        //Update List
    }

    @Override
    protected void onResume() {
        super.onResume();
        int response = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (response != ConnectionResult.SUCCESS) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "onResume: Some error");
            }
            GooglePlayServicesUtil.getErrorDialog(response, this, REQUEST_CODE).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_settings:
                intent = new Intent(this, SettingsActivity.class);
                //noinspection unchecked
                ActivityCompat.startActivity(this,
                        intent,
                        ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle());
                return true;
            case R.id.action_refresh:
                if (mSwipe != null) {
                    mSwipe.setRefreshing(true);
                }
                syncRefresh();
                return true;
            case R.id.action_about:
                intent = new Intent(this, AboutActivity.class);
                //noinspection unchecked
                ActivityCompat.startActivity(this,
                        intent,
                        ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle());
                return true;
            default:
                return false;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = new Uri.Builder()
                .scheme(DbContract.Posts.SCHEME)
                .authority(DbContract.Posts.AUTHORITY)
                .path(DbContract.Posts.TABLE_NAME)
                .build();
        String[] projection = {
                DbContract.Posts._ID,
                DbContract.Posts.KEY_DATE,
                DbContract.Posts.KEY_TITLE,
                DbContract.Posts.KEY_LINK
        };
        String orderBy = DbContract.Posts.KEY_DATE + " DESC";
        return new CursorLoader(this, uri, projection, null, null, orderBy);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swap(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("notifications_enabled")) {
            ManagedChannel channel = ManagedChannelBuilder.forAddress(getString(R.string.address), 50034)
                    .usePlaintext(true)
                    .build();
            final UpdatesGrpc.UpdatesBlockingStub stub = UpdatesGrpc.newBlockingStub(channel);
            String token = sharedPreferences.getString(TOKEN, null);
            if (token != null) {

                final Rupdates.RegisterRequest req = new Rupdates.RegisterRequest();
                req.token = token;
                final boolean register = sharedPreferences.getBoolean(key, true);
                if (register != isRegistered) {
                    //Register
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Rupdates.RegisterReply rep;
                                if (register) {
                                    rep = stub.register(req);
                                } else {
                                    rep = stub.unregister(req);
                                }
                                if (rep.status != 0) {
                                    mHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (register) {
                                                MainActivity.this.showToast(R.string.failed_reg);
                                            } else {
                                                MainActivity.this.showToast(R.string.failed_unreg);
                                            }
                                        }
                                    });
                                } else {
                                    isRegistered = register;
                                }
                            } catch (StatusRuntimeException e) {
                                if (BuildConfig.DEBUG) {
                                    Log.e(TAG, "onSharedPreferenceChanged: ", e);
                                }
                            }
                        }
                    }).start();
                }

            }
        }
    }

    /**
     * Creates the handler used to post to the UI thread
     */
    private void initHandler() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    /**
     * Synchronizes the refresh, so that it doesn't trigger multiple times.
     */
    public void syncRefresh() {
        if (!isRefreshing) {
            synchronized (MainActivity.class) {
                if (!isRefreshing) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "syncRefresh: Refreshing");
                    }
                    isRefreshing = true;
                    refreshList();
                }
            }
        }
    }

    /**
     * Just establishes the swipe view & behavior
     */
    private void initSwipeRefresh() {
        mSwipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "onRefresh: Triggered");
                }
                syncRefresh();
            }
        });
    }

    /**
     * This makes a call out to the server via a non blocking gRPC stub. As the posts come back from
     * the server it adds each one to the database. Once it is complete, it makes the swipe to
     * refresh icon go away again.
     */
    private void refreshList() {
        final ManagedChannel channel = ManagedChannelBuilder.forAddress(getString(R.string.address),50034)
                .usePlaintext(true)
                .build();
        final UpdatesGrpc.UpdatesStub stub = UpdatesGrpc.newStub(channel);
        Rupdates.PostsRequest req = new Rupdates.PostsRequest();
        try {
            stub.getPosts(req, new StreamObserver<Rupdates.PostsReply>() {
                @Override
                public void onNext(Rupdates.PostsReply value) {
                    insertIfNotFound(MainActivity.this,
                            value.link,
                            value.date,
                            value.message);
                }

                @Override
                public void onError(Throwable t) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            isRefreshing = false;
                            if (mSwipe != null) {
                                mSwipe.setRefreshing(false);
                            }
                        }
                    });
                }

                @Override
                public void onCompleted() {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "onCompleted: got all the posts");
                    }
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            isRefreshing = false;
                            if (mSwipe != null) {
                                mSwipe.setRefreshing(false);
                            }
                        }
                    });
                }
            });
        } catch (StatusRuntimeException e) {
            if (BuildConfig.DEBUG)  {
                Log.e(TAG, "refreshList: ", e);
            }
        }
    }

    /**
     * Sets up the action bar as the toolbar
     */
    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    /**
     * If the token has already been created, this does nothing. Otherwise, it kicks off the
     * RegistrationService (IntentService) to create a token, and then send it to the server so that
     * this app can get notifications.
     */
    private synchronized void initTokenIfNeeded() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String token = preferences.getString(TOKEN, null);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "initTokenIfNeeded: " + token);
        }
        if (token == null) {
            Intent intent = new Intent(this, RegistrationService.class);
            startService(intent);
        }
    }

    /**
     * Establishes the recycler view & adapter. Also sets up the layout manager.
     */
    private void initRecyclerView() {
        mAdapter = new PostsAdapter(null);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.posts_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,
                false));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(true);
        getSupportLoaderManager().initLoader(0, null, this);
    }

    /**
     * Uses the given text input to create a toast & shows it.
     *
     * I would prefer to do a snackbar, but it needs to show anywhere, so toast it is.
     */
    private void showToast(@StringRes int stringRes) {
        Toast.makeText(MainActivity.this, stringRes, Toast.LENGTH_SHORT).show();
    }

    /**
     * Inserts a post into the database. First checks to see if the post already exists. If so, it
     * does nothing, otherwise it will add this post.
     */
    public static void insertIfNotFound(Context context, String link, String date, String title) {
        final Uri uri = new Uri.Builder()
                .scheme(DbContract.Posts.SCHEME)
                .authority(DbContract.Posts.AUTHORITY)
                .path(DbContract.Posts.TABLE_NAME)
                .build();
        final String[] projection = {
                DbContract.Posts._ID,
                DbContract.Posts.KEY_DATE,
                DbContract.Posts.KEY_TITLE,
                DbContract.Posts.KEY_LINK
        };
        String selection = DbContract.Posts.KEY_LINK + " = ?";
        String[] selectionArgs = {link};
        Cursor cursor = context.getContentResolver().query(uri,
                projection,
                selection,
                selectionArgs,
                null
        );
        if (cursor == null || cursor.getCount() < 1) {
            ContentValues values = new ContentValues();
            values.put(DbContract.Posts.KEY_DATE, date);
            values.put(DbContract.Posts.KEY_TITLE, title);
            values.put(DbContract.Posts.KEY_LINK, link);
            context.getContentResolver().insert(uri, values);
        }
        if (cursor != null) {
            cursor.close();
        }
    }
}
