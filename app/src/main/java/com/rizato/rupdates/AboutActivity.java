package com.rizato.rupdates;

/*
Copyright 2016 Robert Lathrop

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import de.psdev.licensesdialog.LicensesDialog;
import de.psdev.licensesdialog.licenses.ApacheSoftwareLicense20;
import de.psdev.licensesdialog.licenses.BSD3ClauseLicense;
import de.psdev.licensesdialog.licenses.CreativeCommonsAttributionNoDerivs30Unported;
import de.psdev.licensesdialog.licenses.License;
import de.psdev.licensesdialog.model.Notice;
import de.psdev.licensesdialog.model.Notices;

/**
 * This activity shows the about page for my application. It just gives a few words giving
 * attribution to third party libraries, and mentions the wyvern community.
 */
public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initActionBar();
        initSubReddit();
        initWebsite();
        initLicenses();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ActivityCompat.finishAfterTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Sets up the button click to open the subreddit
     */
    private void initSubReddit() {
        Button websiteButton = (Button) findViewById(R.id.subreddit);
        websiteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.subreddit)));
                startActivity(intent);
            }
        });
    }

    /**
     * Sets up the button click to open my website
     */
    private void initWebsite() {
        Button websiteButton = (Button) findViewById(R.id.website);
        websiteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.website)));
                startActivity(intent);
            }
        });
    }

    /**
     * Sets up the button click to open the licenses activity
     */
    private void initLicenses() {
        Button licenseButton = (Button) findViewById(R.id.license);
        licenseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadLicenses();
            }
        });
    }

    /**
     * This is hideous. But, I needed to build all the licenses, and this is how you do it with this
     * guys library from github.
     */
    private void loadLicenses() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Notices notice = new Notices();
                notice.addNotice(new Notice("gRPC OkHTTP",
                        "http://www.grpc.io/",
                        "2014 Google Inc.",
                        new BSD3ClauseLicense()));
                notice.addNotice(new Notice("gRPC Protobuf Nano",
                        "http://www.grpc.io/",
                        "2014 Google Inc.",
                        new BSD3ClauseLicense()));
                notice.addNotice(new Notice("gRPC Stub",
                        "http://www.grpc.io/",
                        "2014 Google Inc.",
                        new BSD3ClauseLicense()));
                notice.addNotice(new Notice("Licenses Dialog",
                        "http://psdev.de",
                        "2013 Phillip Schiffer", new ApacheSoftwareLicense20()));
                notice.addNotice(new Notice("Support AppCompat-v7",
                        "http://developer.android.com/index.html",
                        "2011 The Android Open Source Project",
                        new ApacheSoftwareLicense20()));
                notice.addNotice(new Notice("Support Design",
                        "http://developer.android.com/index.html",
                        "2011 The Android Open Source Project",
                        new ApacheSoftwareLicense20()));
                notice.addNotice(new Notice("Support Recyclerview",
                        "http://developer.android.com/index.html",
                        "2011 The Android Open Source Project",
                        new ApacheSoftwareLicense20()));
                notice.addNotice(new Notice("Support Preference",
                        "http://developer.android.com/index.html",
                        "2011 The Android Open Source Project",
                        new ApacheSoftwareLicense20()));
                notice.addNotice(new Notice("Google Play Services",
                        "https://developers.google.com/android/guides/overview",
                        "2005- 2008 The Android Open Source Project",
                        new ApacheSoftwareLicense20()));
                notice.addNotice(new Notice("Refresh Icon",
                        "https://design.google.com/icons/",
                        "",
                        new License() {
                            @Override
                            public String getName() {
                                return "CC-BY 4.0";
                            }

                            @Override
                            public String readSummaryTextFromResources(Context context) {
                                return "http://creativecommons.org/licenses/by/4.0/legalcode";
                            }

                            @Override
                            public String readFullTextFromResources(Context context) {
                                return null;
                            }

                            @Override
                            public String getVersion() {
                                return null;
                            }

                            @Override
                            public String getUrl() {
                                return "http://creativecommons.org/licenses/by/4.0/legalcode";
                            }
                        }));
                new LicensesDialog.Builder(AboutActivity.this)
                        .setNotices(notice)
                        .build()
                        .show();
            }
        });
    }

    /**
     * Sets up the toolbar with a title, and back behavior.
     */
    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.about);
        }
    }
}
