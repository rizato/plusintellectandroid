package com.rizato.rupdates;

/*
Copyright 2016 Robert Lathrop

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rizato.rupdates.database.DbContract;
import com.rizato.rupdates.util.HumanReadable;

/**
 * This is a pretty simple adapter for the recycler view. It stores a cursor, and when the viewholder
 * needs to attach a view to a viewHolder, this just grabs values from the cursor and puts them into
 * the appropriate view widgets in the item view.
 */
public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {
    private Cursor mCursor;

    public PostsAdapter(Cursor cursor) {
        mCursor = cursor;
    }

    public Cursor swap(Cursor cursor) {
        Cursor old = mCursor;
        mCursor = cursor;
        notifyDataSetChanged();
        return old;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Ignore viewtype, cause we only have one.
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.post, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String title = null;
        String link = null;
        Long date = null;
        //Grabbing values from the cursor
        mCursor.moveToPosition(position);
        int index = mCursor.getColumnIndex(DbContract.Posts.KEY_DATE);
        if (index >= 0) {
            date = mCursor.getLong(index);
        }
        index = mCursor.getColumnIndex(DbContract.Posts.KEY_LINK);
        if (index >= 0) {
            link = mCursor.getString(index);
        }
        index = mCursor.getColumnIndex(DbContract.Posts.KEY_TITLE);
        if (index >= 0) {
            title = mCursor.getString(index);
        }
        if (holder.title != null) {
            holder.title.setText(title);
        }
        if (holder.date != null && date != null) {
            holder.date.setText(HumanReadable.date(holder.date.getContext(), date));
        }
        if (holder.itemView != null) {
            final String l = link;
            if (l != null) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.reddit.com" + l));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(intent);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return mCursor != null ? mCursor.getCount() : 0;
    }

    /**
     * This a viewholder object. It does the findByViewId methods when the view is first bound to this
     * holder so that we don't take a performance penalty when scrolling.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView date;
        View itemView;
        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            title = (TextView) itemView.findViewById(android.R.id.text1);
            date = (TextView) itemView.findViewById(R.id.date);
        }
    }
}
