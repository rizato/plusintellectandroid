package com.rizato.rupdates.database;

/*
Copyright 2016 Robert Lathrop

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import android.provider.BaseColumns;

/**
 * This just establishes any table's columns in an easy to use way.
 */
public class DbContract {
    /**
     * This defines the columns & uri for the post table. By implemting BaseColumns we also get
     * the _ID column and _COUNT.
     */
    public static class Posts implements BaseColumns{
        public static final String AUTHORITY = "com.rizato.rupdates.provider";
        public static final String SCHEME = "content";
        public static final String TABLE_NAME = "posts";
        public static final String KEY_LINK = "link";
        public static final String KEY_TITLE = "title";
        public static final String KEY_DATE = "date";
    }
}
