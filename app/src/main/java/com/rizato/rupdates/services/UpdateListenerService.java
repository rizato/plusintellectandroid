package com.rizato.rupdates.services;

/*
Copyright 2016 Robert Lathrop

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.rizato.rupdates.BuildConfig;
import com.rizato.rupdates.MainActivity;
import com.rizato.rupdates.R;

/**
 * This listens for messages from the google cloud messenger. These messages will contain four fields
 *
 * link - A link to the post on Reddit
 * title - The title of the post on Reddit
 * date - The date/time of the post of Reddit
 */
public class UpdateListenerService extends GcmListenerService {
    private static final String LINK = "link";
    private static final String TITLE = "title";
    private static final String DATE = "date";
    private static final String MESSAGE = "message";
    private static final String TAG = UpdateListenerService.class.getSimpleName();
    private static final int REQUEST = 3843;
    private static final int ID = 3844;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onMessageReceived: Test");
        }
        String link = data.getString(LINK);
        String title = data.getString(TITLE);
        String date = data.getString(DATE);
        String message = data.getString(MESSAGE);
        //Write title & link to database
        MainActivity.insertIfNotFound(this, link, date, title);

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pending = PendingIntent.getActivity(this, REQUEST, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        Notification notificationCompat = builder.setContentText(title)
                .setContentTitle(message)
                .setSmallIcon(R.drawable.ic_hat_notify_24dp)
                .setAutoCancel(true)
                .setContentIntent(pending)
                .build();
        NotificationManagerCompat.from(this).notify(ID, notificationCompat);
    }
}