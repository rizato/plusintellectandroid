package com.rizato.rupdates.util;

/*
Copyright 2016 Robert Lathrop

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import android.content.Context;
import android.util.Log;

import com.rizato.rupdates.BuildConfig;
import com.rizato.rupdates.R;

import java.util.Calendar;

/**
 * This class holds some static methods to make things more human readable. While the times aren't
 * exact, they are fun approximations of how long it has actually been.
 */
public class HumanReadable {
    private static final long JUST_NOW = 10000;
    private static final long A_MOMENT_AGO = 180000;
    private static final long A_FEW_MOMENTS_AGO = 1800000;
    private static final long AN_HOUR_AGO = 5400000;
    private static final long OVER_AN_HOUR_AGO = 43200000;
    private static final long A_DAY_AGO = 129600000;
    private static final long WEEK = 1000 * 60 * 60 * 24 * 7;
    private static final String TAG = HumanReadable.class.getSimpleName();


    /**
     * This takes in a context and date in millis. It spits out a string that is easier than an
     * actual date.
     */
    public static String date(Context context, Long date) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "date: " + String.valueOf(date));
        }
        if (date == null) {
            return "Unknown";
        }
        long now = Calendar.getInstance().getTimeInMillis();
        long diff = now-date;
        Calendar nye = Calendar.getInstance();
        int year = nye.get(Calendar.YEAR);
        nye.set(year, 0,0,0,0,0);
        if (diff < JUST_NOW) {
            return context.getString(R.string.just_now);
        } else if (diff < A_MOMENT_AGO ) {
            return context.getString(R.string.moment);
        } else if (diff < A_FEW_MOMENTS_AGO) {
            return context.getString(R.string.moments);
        } else if (diff < AN_HOUR_AGO) {
            return context.getString(R.string.an_hour);
        } else if (diff < OVER_AN_HOUR_AGO) {
            return context.getString(R.string.over_hour);
        } else if (diff < A_DAY_AGO) {
            return context.getString(R.string.day_ago);
        } else {
            long weeks = diff/ WEEK;
            if (weeks == 0) {
                return context.getString(R.string.less_week);
            } else {
                return String.format(context.getString(R.string.weeks), weeks);
            }
        }
    }
}
