package com.rizato.rupdates.database;

/*
Copyright 2016 Robert Lathrop

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Creates the database & drops in on updates. (I will use a better solution if I ever actually
 * change the database)
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String name = "updates";
    private static final int version = 3;

    private static final String CREATE_POSTS = "CREATE TABLE "+DbContract.Posts.TABLE_NAME
            + " ("
            + DbContract.Posts._ID + " INTEGER PRIMARY KEY, "
            + DbContract.Posts.KEY_DATE + " INTEGER, "
            + DbContract.Posts.KEY_LINK + " TEXT, "
            + DbContract.Posts.KEY_TITLE + " TEXT )";

    private static final String DROP_POSTS = "DROP TABLE "+ DbContract.Posts.TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_POSTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_POSTS);
        db.execSQL(CREATE_POSTS);
    }
}
