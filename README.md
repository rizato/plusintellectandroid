# Android Plus Intellect

So I realize that is a terrible name. It stems from one of my other apps called Plus Strength.

Anyhow, I built this so that I wouldn't have to check the [/r/wyvernrpg](https://reddit.com/r/wyvernrpg) subreddit as often as I have been. This works in conjunction with the server to show a list of Rhialto's posts on /r/wyvernrpg and to show notifications when  new post is made. (Or at least show one by 10 minutes after a post is made.)

If you build and run this it should be preconfigured to work with my server which is up and running as of 3/11. I may take it down at some point though. I only have 2 free months of hosting. Then again, it is only $6 a month after that, so I may leave it.

# Building

* Get Android Studio
* Clone the project from Android Studio
* Attach a phone
* Run

# Sorry

Apologies for the terrible README files. I will flesh them out later. 


```
Copyright 2016 Robert Lathrop

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```