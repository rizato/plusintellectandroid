package com.rizato.rupdates.database;

/*
Copyright 2016 Robert Lathrop

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.HashMap;

/**
 * This is a super simple content provider. We are only handling insertion and queries. There is
 * only one table, and only 3 columns so it is pretty simple.
 */
public class PostsContentProvider extends ContentProvider {
    private SQLiteDatabase mDb;

    private static UriMatcher sUriMatcher;
    private static HashMap<Integer, String> sMimeTypes;

    private static final int POSTS = 1;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(DbContract.Posts.AUTHORITY, DbContract.Posts.TABLE_NAME, POSTS);


        sMimeTypes = new HashMap<>();
        sMimeTypes.put(POSTS, "vnd.android.cursor.dir/vnd.com.rizato.rupdates.posts");
    }

    @Override
    public boolean onCreate() {
        mDb = new DatabaseHelper(getContext()).getWritableDatabase();
        return false;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        switch (sUriMatcher.match(uri)) {
            case POSTS:
                //Just pulling the posts out of the database
                if (mDb != null) {
                    Cursor cursor = mDb.query(DbContract.Posts.TABLE_NAME,
                            projection,
                            selection,
                            selectionArgs,
                            null,
                            null,
                            sortOrder);
                    Context c = getContext();
                    if (c != null) {
                        cursor.setNotificationUri(c.getContentResolver(), uri);
                    }
                    return cursor;
                }
            default:
                return null;
        }
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        switch (sUriMatcher.match(uri)) {
            case POSTS:
                //Adding posts in.
                Context c = getContext();
                long id = mDb.insert(DbContract.Posts.TABLE_NAME, null, values);
                if (c != null) {
                    c.getContentResolver().notifyChange(uri, null);
                }
                return Uri.withAppendedPath(uri, String.valueOf(id));
            default:
                return null;
        }
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return sMimeTypes.get(sUriMatcher.match(uri));
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        //We are not handling deletions
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        //Not implementing this.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
