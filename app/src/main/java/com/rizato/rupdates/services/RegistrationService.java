package com.rizato.rupdates.services;

/*
Copyright 2016 Robert Lathrop

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

import android.app.IntentService;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.rizato.rupdates.BuildConfig;
import com.rizato.rupdates.MainActivity;
import com.rizato.rupdates.R;
import com.rizato.rupdates.nano.*;
import com.rizato.rupdates.nano.Rupdates;

import java.io.IOException;
import java.net.ConnectException;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

/**
 * This creates a token for GCM, and then sends it up to the server. If the server accepted the token,
 * then this service will store the token in preferences so that it can be stored for later runs of
 * the application.
 */
public class RegistrationService extends IntentService {
    private static final String TAG = RegistrationService.class.getSimpleName();

    public RegistrationService() {
        super(RegistrationService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onHandleIntent: Handling an intent");
        }
        long sleep = 1000;
        InstanceID instanceID = InstanceID.getInstance(this);
        String token;
        while (true) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "onHandleIntent: Attempting to register token");
            }
            try {
                //Creating token
                token = instanceID.getToken(getString(R.string.sender_id),
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE,
                        null);
                ManagedChannel channel = ManagedChannelBuilder.forAddress(getString(R.string.address), 50034)
                        .usePlaintext(true)
                        .build();
                UpdatesGrpc.UpdatesBlockingStub blockingStub = UpdatesGrpc.newBlockingStub(channel);
                com.rizato.rupdates.nano.Rupdates.RegisterRequest req = new Rupdates.RegisterRequest();
                req.token = token;
                Rupdates.RegisterReply reply =  blockingStub.register(req);
                if (reply.status == 0) {
                    //Storing the token
                    PreferenceManager.getDefaultSharedPreferences(this)
                            .edit()
                            .putString(MainActivity.TOKEN, token)
                            .apply();
                    break;
                }
            } catch (IOException | StatusRuntimeException e) {
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "onHandleIntent: error", e);
                }
            }
            //Exponential backoff
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException ignore) {
            }
            sleep = sleep *2;

        }
    }
}